package codeforces263;

import java.util.Scanner;

/**
 * Created by amayorov on 26.08.2014.
 */
public class Codeforces462A {

	static boolean isZero(boolean[][] m, int i, int j) {
		if ((i >= 0) && (i < m.length)) {
			if ((j >= 0) && (j < m.length)) {
				return !m[i][j];
			}
		}

		return false;
	}

	static int countZeros(boolean[][] m, int i, int j) {

		int result = 0;
		result += isZero(m, i - 1, j) ? 1 : 0;
		result += isZero(m, i + 1, j) ? 1 : 0;
		result += isZero(m, i, j - 1) ? 1 : 0;
		result += isZero(m, i, j + 1) ? 1 : 0;

		return result;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		sc.nextLine();

		boolean[][] m = new boolean[n][n];

		for(int i = 0; i < n; ++i) {
			String str = sc.nextLine();
			for (int j = 0; j < n; ++j) {
				char c = str.charAt(j);
				if (c == 'x') {
					m[i][j] = true;
				} else {
					m[i][j] = false;
				}
			}
		}

		for(int i = 0; i < n; ++i) {
			for(int j = 0; j < n; ++j) {
				if (countZeros(m, i, j) % 2 != 0) {
					System.out.println("NO");
					return;
				}
			}
		}

		System.out.println("YES");
	}

}
