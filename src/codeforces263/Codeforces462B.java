package codeforces263;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by amayorov on 26.08.2014.
 */
public class Codeforces462B {

	static Map.Entry<Character, AtomicInteger> getBestEntryFromMap(Map<Character, AtomicInteger> map) {
		Map.Entry<Character, AtomicInteger> maxEntry = null;

		for (Map.Entry<Character, AtomicInteger> entry : map.entrySet())
		{
			if (maxEntry == null || entry.getValue().get() > maxEntry.getValue().get())
			{
				maxEntry = entry;
			}
		}

		if (maxEntry != null) {
			map.remove(maxEntry.getKey());
		}

		return maxEntry;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int k = sc.nextInt();

		Map<Character, AtomicInteger> map = new HashMap<>(50);

		sc.nextLine();
		String str = sc.nextLine();

		for(int i = 0; i < n; ++i) {
			Character c = str.charAt(i);
			if (map.get(c) == null) {
				map.put(c, new AtomicInteger(0));
			}
			map.get(c).incrementAndGet();
		}

		long result = 0;

		while (k > 0) {
			long v = Math.min(getBestEntryFromMap(map).getValue().get(), k);
			result += v * v;
			k -= v;
		}


		System.out.println(result);
	}

}
