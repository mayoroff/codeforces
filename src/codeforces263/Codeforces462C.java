package codeforces263;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by amayorov on 26.08.2014.
 */
public class Codeforces462C {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();

		int[] arr = new int[n];

		for(int i = 0; i < n; ++i) {
			arr[i] = sc.nextInt();
		}

		Arrays.sort(arr);

		long sum = 0;
		long psum = 0;

		for(int i = 0; i < n; ++i) {
			psum += arr[i];
		}
		sum += psum;

		for(int i = 0; i < n; ++i) {
			int v = arr[i];
			psum -= v;

			if (psum == 0) {
				break;
			}

			sum += v;
			sum += psum;
		}

		System.out.println(sum);

	}

}
