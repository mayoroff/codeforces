package random_solving;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by amayorov on 06.05.15.
 */

//Todo does not work!

public class CodeForces339E {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();

		int[] seq = new int[n];

		for(int i = 0; i < n; ++i) {
			seq[i] = sc.nextInt();
		}

		Set<Integer> starts = findSpecialPoints(seq);
		//Set<Integer> ends = findSpecialPointsForEnds(starts, seq.length - 1);
		//ends.addAll(starts);

		List<Segment> result = new ArrayList<>();

		int i = 1;

		int[] test = new int[n];
		initArray(test);

		//starts.clear();
		//ends.clear();
		//starts.addAll(Arrays.asList(6, 10, 13, 9, 12, 14));
		//ends.addAll(Arrays.asList(6, 10, 13, 9, 12, 14));

		if (!brute(i, seq, starts, starts, result, test)) {
			System.out.println("0");
		}

	}

	private static boolean brute(int i, int[] seq, Set<Integer> starts, Set<Integer> ends, List<Segment> result, int[] test) {

		if (i > 3) {
			return false;
		}

		for(Integer l: starts) {
			for(Integer r: ends) {
				if (l >= r) {
					continue;
				}
				applyInversion(test, new Segment(l, r));
				result.add(new Segment(l, r));
				if (Arrays.equals(test, seq)) {
					System.out.println(result.size());
					for(Segment s: result) {
						System.out.println((s.start + 1) + " " + (s.end + 1));
					}
					return true;
				}
				if (brute(i+1, seq, starts, ends, result, test)) {
					return true;
				}
				applyInversion(test, new Segment(l, r));
				result.remove(result.size() - 1);
			}
		}

		return false;
	}

	private static void initArray(int[] test) {
		for(int i = 0; i < test.length; ++i) {
			test[i] = i+1;
		}
	}

	private static Set<Integer> findSpecialPointsForEnds(Set<Integer> starts, int n) {
		Set<Integer> result = new HashSet<>();
		for(Integer i: starts) {
			if (i > 1) {
				result.add(i - 1);
			}
		}
		result.add(n);
		return result;
	}

	private static Set<Integer> findSpecialPoints(int[] seq) {
		Set<Integer> result = new HashSet<>();
		boolean flag = false;
		for (int i = 0; i < seq.length; i++) {
			if ((seq[i] == (i+1)) == flag) {
				result.add(flag ? i - 1 : i);
				flag = !flag;
			}
		}

		return result;
	}


	public static void applyInversion(int[] seq, Segment inversion) {
		for(int i = inversion.start; i < inversion.start + (inversion.end - inversion.start + 1)/2; ++i) {
			int tmp = seq[i];
			seq[i] = seq[inversion.end - (i - inversion.start)];
			seq[inversion.end - (i - inversion.start)] = tmp;
		}
	}



}


class Segment {
	public int start;
	public int end;

	public Segment() {
	}

	public Segment(int start, int end) {
		this.start = start;
		this.end = end;
	}
}
