package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 05.05.15.
 */
public class Codeforces339B {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int m = sc.nextInt();

		int[] arr = new int[m];
		for(int i = 0; i < m; ++i) {
			arr[i] = sc.nextInt();
		}

		long result = 0;
		int state = 1;

		for(int i = 0; i < m; ++i) {
			int move = arr[i] - state;
			if (move >= 0) {
				result += move;
			} else {
				result += move + n;
			}
			state = arr[i];
		}

		System.out.println(result);

	}

}
