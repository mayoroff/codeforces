package random_solving;

import java.util.Scanner;

/**
 * Created by Arseny Mayorov on 05.10.2014.
 */
public class Codeforces472A {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        if (n % 2 == 0) {
            System.out.println("8" + " " + (n-8));
        } else {
            System.out.println("9" + " " + (n-9));
        }

    }

}
