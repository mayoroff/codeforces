package random_solving;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created by amayorov on 09.09.2014.
 */
public class Codeforces220A {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		List<Integer> arr = new ArrayList<>(n);
		for(int i = 0; i < n; ++i) {
			arr.add(sc.nextInt());
		}


		List<Integer> sorted = new ArrayList<>(arr);


		Collections.sort(sorted);

		int diff = 0;

		for(int i = 0; i < n; ++i) {
			if (!arr.get(i).equals(sorted.get(i))) {
				diff++;
			}
		}

		if (diff > 2) {
			System.out.println("NO");
		} else {
			System.out.println("YES");
		}
	}

}
