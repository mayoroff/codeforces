package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 25.05.15.
 */
public class Codeforces23B {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		//int[] req = new int[n];
		for(int i = 0; i < n; ++i) {
			int k = sc.nextInt();
			int r = (k / 4 << 1) + k % 4 / 3;
			System.out.println(r);
		}

	}

}
