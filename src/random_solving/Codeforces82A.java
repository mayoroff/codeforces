package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 12.05.15.
 */
public class Codeforces82A {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();

		String[] arr = {"Sheldon", "Leonard", "Penny", "Rajesh", "Howard"};

		int m = 1;

		while (n > 0) {
			n -= m * 5;
			m *= 2;
		}

		int k = n + ((m/2)*5);
		int result = (k-1) / (m/2);

		System.out.println(arr[result]);

	}


}
