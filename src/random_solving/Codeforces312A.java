package random_solving;

import java.util.Scanner;

/**
 * Created by Arseni on 11.07.2014.
 */
public class Codeforces312A {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        sc.nextLine();
        for(int i = 0; i < n; ++i) {
            String str = sc.nextLine();
            if (str.startsWith("miao.") && !str.endsWith("lala.")) {
                System.out.println("Rainbow's");
                continue;
            }
            if (!str.startsWith("miao.") && str.endsWith("lala.")) {
                System.out.println("Freda's");
                continue;
            }
            System.out.println("OMG>.< I don't know!");
        }


    }
}
