package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 05.09.2014.
 */
public class Codeforces463B {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();

		int energy = 0;

		for(int i = 0; i < n; ++i) {
			energy = Math.max(sc.nextInt(), energy);
		}

		System.out.println(energy);
	}

}
