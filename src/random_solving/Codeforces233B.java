package random_solving;

import java.util.Scanner;
import java.util.function.Function;


/**
 * Created by amayorov on 27.08.2014.
 */

public class Codeforces233B {

	private static long functionalBinarySearch(Function<Long, Long> function, long searchValue, long left, long right) {
		if (right < left) {
			//throw new IllegalArgumentException();
			return left;
		}
		/*
		int mid = mid = (left + right) / 2;
		There is a bug in the above line;
		Joshua Bloch suggests the following replacement:
		*/
		long mid = (left + right) >>> 1;
		long fMid = function.apply(mid);

		if (searchValue > fMid) {
			return functionalBinarySearch(function, searchValue, mid + 1, right);
		} else if (searchValue < fMid) {
			return functionalBinarySearch(function, searchValue, left, mid - 1);
		} else {
			return mid;
		}
	}

	public static int sumOfDigits(long x) {
		int result = 0;

		while (x != 0) {
			result += x % 10;
			x /= 10;
		}

		return result;
	}

	public static long equation(long x) {
		return x*x + sumOfDigits(x) * x;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		long n = sc.nextLong();

		long result;
		if (n <= 10000) {
			result = bruteforce(n, 0, (long) (Math.sqrt(n) + 1));
		} else {
			result = functionalBinarySearch(Codeforces233B::equation, n, 0, (long)Math.sqrt(n));

			if (n != equation(result)) {
				result = bruteforce(n, result - 1000, result + 1000);
			}
		}

		System.out.println(result);

	}

	private static long bruteforce(long n, long startValue, long endValue) {
		for(long i = startValue; i <= endValue; ++i) {
			if (equation(i) == n) {
				return i;
			}
		}
		return -1;
	}

}
