package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 24.09.2014.
 */
public class Codeforces255A {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();

		int[] arr = new int[3];

		for(int i = 0; i < n; ++i) {
			arr[i%3] += sc.nextInt();
		}

		if ((arr[0] > arr[1]) && (arr[0] > arr[2])) {
			System.out.println("chest");
		}
		if ((arr[1] > arr[0]) && (arr[1] > arr[2])) {
			System.out.println("biceps");
		}
		if ((arr[2] > arr[0]) && (arr[2] > arr[1])) {
			System.out.println("back");
		}
	}

}
