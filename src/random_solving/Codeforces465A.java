package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 08.09.2014.
 */
public class Codeforces465A {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		sc.nextLine();
		String str = sc.nextLine();
		int d = str.indexOf('0');
		if (d == -1) {
			d = n;
		} else {
			d +=1;
		}
		System.out.println(d);

	}

}
