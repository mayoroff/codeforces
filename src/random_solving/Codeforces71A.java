package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 23.09.2014.
 */
public class Codeforces71A {


	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		sc.nextLine();

		for (int i = 0; i < n; ++i) {
			String str = sc.nextLine();
			if (str.length() > 10) {
				System.out.println("" + str.charAt(0) + (str.length() - 2) + str.charAt(str.length() - 1));
			} else {
				System.out.println(str);
			}
		}
	}

}
