package random_solving;

import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by Arseni on 13.07.2014.
 */
public class Codeforces447B {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        int k = sc.nextInt();

        HashMap<String, Integer> w = new HashMap<>();

        int max = 0;
        for(char c = 'a'; c <= 'z'; ++c) {
            int g = sc.nextInt();
            w.put(String.valueOf(c), g);
            max = Math.max(max, g);
        }

        int original = 0;
        int i;
        for(i = 0; i < str.length(); ++i) {
            original += (i+1) * w.get(String.valueOf(str.charAt(i)));
        }

        for(int f = 0; f < k; f++, i++ ) {
            original += (i+1) * max;
        }


        System.out.println(original);

    }

}
