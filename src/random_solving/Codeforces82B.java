package random_solving;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by amayorov on 12.05.15.
 */
public class Codeforces82B {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		NumberInfo[] arr = new NumberInfo[201];

		for(int i = 0; i < 201; ++i) {
			arr[i] = new NumberInfo();
		}

		HashSet<Set<Integer>> result = new HashSet<>();


		int n = sc.nextInt();
		int end = n*(n-1)/2;

		if (n == 2) {
			int k = sc.nextInt();
			System.out.println("1 " + sc.nextInt());
			StringBuilder sb = new StringBuilder();
			sb.append(k-1);
			for(int i = 0; i < k - 1; ++i) {
				sb.append(" ");
				sb.append(sc.nextInt());
			}
			System.out.println(sb.toString());
			return;
		}

		for(int i = 0; i < end; ++i) {
			int k = sc.nextInt();
			HashMap<Integer, Set<Integer>> tmp = new HashMap<>();
			for(int j = 0; j < k; ++j) {


				int a = sc.nextInt();
				if (arr[a].occurences == 0) {
					arr[a].occurences = 1;
					arr[a].place = i;
				} else {
					arr[a].occurences = 0;
					if (tmp.get(arr[a].place) == null) {
						HashSet<Integer> e = new HashSet<>();
						e.add(a);
						tmp.put(arr[a].place, e);
					} else {
						tmp.get(arr[a].place).add(a);
					}

				}
			}
			result.addAll(tmp.values());
		}

		for(Set<Integer> r: result) {
			StringBuilder sb = new StringBuilder();
			sb.append(r.size());
			sb.append(" ");
			for(Integer i: r) {
				sb.append(i);
				sb.append(" ");
			}
			System.out.println(sb.toString());
		}

	}

}

class NumberInfo {
	int occurences = 0;
	int place = 0;
}
