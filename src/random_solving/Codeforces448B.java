package random_solving;

import java.util.Scanner;

/**
 * Created by Arseni on 19.07.2014.
 */
public class Codeforces448B {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int y = sc.nextInt();
        int n = sc.nextInt();

        long start = System.nanoTime();

        int f = y;
        int f0 = x;

        if (n == 1) {
            f = x;
        } else if (n == 2) {
            f = y;
        } else {
            for (int i = 2; i < n; i++) {
                int tmp = f;
                f = f - f0;
                f0 = tmp;
            }
        }

        if (f < 0) {
            f = 1000000007 - Math.abs(f % 1000000007);
        }
        f = f % 1000000007;


        long elapsedTime = System.nanoTime() - start;
        System.err.println((double)elapsedTime / Math.pow(10, 9));

        System.out.println(f);
    }

}
