package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 26.08.2014.
 */

public class Codeforces459A {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int x1 = sc.nextInt();
		int y1 = sc.nextInt();
		int x2 = sc.nextInt();
		int y2 = sc.nextInt();

		int x3;
		int y3;
		int x4;
		int y4;

		if (x1 == x2) {
			y3 = y1;
			y4 = y2;
			x3 = Math.abs(y2 - y1) + x1;
			x4 = x3;
		} else if (y1 == y2) {
			x3 = x1;
			x4 = x2;
			y3 = Math.abs(x2 - x1) + y1;
			y4 = y3;
		} else if ((x2 - x1) == (y2 - y1)) {
			x3 = Math.min(x1, x2);
			y3 = Math.max(y1, y2);
			x4 = Math.max(x1, x2);
			y4 = Math.min(y1, y2);
		} else if ((x2 - x1) == (y1 - y2)) {
			x3 = Math.min(x1, x2);
			y3 = Math.min(y1, y2);
			x4 = Math.max(x1, x2);
			y4 = Math.max(y1, y2);
		}	else  {
			System.out.println("-1");
			return;
		}

		System.out.println(x3 + " " + y3 + " " + x4 + " " + y4);
	}
}
