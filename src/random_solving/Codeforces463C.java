package random_solving;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Arseny Mayorov on 07.09.2014.
 */

//TODO fixme
public class Codeforces463C {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int[][] arr = new int[n][n];

        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < n; ++j) {
                arr[i][j] = sc.nextInt();
            }
        }

        //Find optimum between left white diagonals
        ArrayList<Position> leftWhitePositions = new ArrayList<>();
        ArrayList<Long> leftWhiteSums = new ArrayList<>();
        ArrayList<Position> rightWhitePositions = new ArrayList<>();
        ArrayList<Long> rightWhiteSums = new ArrayList<>();

        ArrayList<Position> leftBlackPositions = new ArrayList<>();
        ArrayList<Long> leftBlackSums = new ArrayList<>();
        ArrayList<Position> rightBlackPositions = new ArrayList<>();
        ArrayList<Long> rightBlackSums = new ArrayList<>();

        for(int i = 0; i < n; ++i) {

            long sum = leftDiagonalSum(arr, n, i, 0);
            if (isWhite(i, 0)) {
                leftWhiteSums.add(sum);
                leftWhitePositions.add(new Position(i, 0));
            } else {
                leftBlackSums.add(sum);
                leftBlackPositions.add(new Position(i, 0));
            }
        }
        for(int j = 0; j < n; ++j) {
            long sum = leftDiagonalSum(arr, n, 0, j);

            if (isWhite(0, j)) {
                leftWhiteSums.add(sum);
                leftWhitePositions.add(new Position(0, j));
            } else {
                leftBlackSums.add(sum);
                leftBlackPositions.add(new Position(0, j));
            }
        }

        for(int i = 0; i < n; ++i) {
            long sum = rightDiagonalSum(arr, n, i, n - 1);

            if (isWhite(i,n-1)) {
                rightWhiteSums.add(sum);
                rightWhitePositions.add(new Position(i, n-1));
            } else {
                rightBlackSums.add(sum);
                rightBlackPositions.add(new Position(i, n-1));
            }
        }
        for(int j = n - 1; j >= 0; --j) {
            long sum = rightDiagonalSum(arr, n, 0, j);
            if (isWhite(0,j)) {
                rightWhiteSums.add(sum);
                rightWhitePositions.add(new Position(0, j));
            } else {
                rightBlackSums.add(sum);
                rightBlackPositions.add(new Position(0, j));
            }
        }

        long whiteMax = -1;
        long whiteX = 0;
        long whiteY = 0;
        for(int i = 0; i < leftWhiteSums.size(); ++i) {
            Position crossing = findCrossing(n, leftWhitePositions.get(i), rightWhitePositions.get(i));

            if (crossing != null) {
                long total = leftWhiteSums.get(i) + rightWhiteSums.get(i) - arr[crossing.x][crossing.y];
                if (total > whiteMax) {
                    whiteMax = total;
                    whiteX = crossing.x;
                    whiteY = crossing.y;
                }
            } else {
                //check without crossing (separately)

                if (leftWhiteSums.get(i) > whiteMax) {
                    whiteMax = leftWhiteSums.get(i);
                    whiteX = leftWhitePositions.get(i).x;
                    whiteY = leftWhitePositions.get(i).y;
                }
                if (rightWhiteSums.get(i) > whiteMax) {
                    whiteMax = rightWhiteSums.get(i);
                    whiteX = rightWhitePositions.get(i).x;
                    whiteY = rightWhitePositions.get(i).y;
                }
            }
        }

        long blackMax = -1;
        long blackX = 0;
        long blackY = 0;
        for(int i = 0; i < rightBlackSums.size(); ++i) {
            Position crossing = findCrossing(n, leftBlackPositions.get(i), rightBlackPositions.get(i));

            if (crossing != null) {
                long total = leftBlackSums.get(i) + rightBlackSums.get(i) - arr[crossing.x][crossing.y];
                if (total > blackMax) {
                    blackMax = total;
                    blackX = crossing.x;
                    blackY = crossing.y;
                }
            } else {
                //check without crossing (separately)

                if (leftBlackSums.get(i) > blackMax) {
                    blackMax = leftBlackSums.get(i);
                    blackX = leftBlackPositions.get(i).x;
                    blackY = leftBlackPositions.get(i).y;
                }
                if (rightBlackSums.get(i) > blackMax) {
                    blackMax = rightBlackSums.get(i);
                    blackX = rightBlackPositions.get(i).x;
                    blackY = rightBlackPositions.get(i).y;
                }
            }
        }

        System.out.println(whiteMax + blackMax);
        System.out.println((whiteX+1) + " " + (whiteY+1) + " " + (blackX+1) + " " + (blackY+1));
    }

    public static Position findCrossing(int n, Position left, Position right) {

        if ((left.x == n-1 && left.y == 0) ||
            (left.x == 0 && left.y == n-1) ||
                (right.x == 0 && right.y == 0) ||
                (right.x == n-1 && right.y == n-1) ||
                (isWhite(left.x, left.y) != isWhite(right.x, right.y))) {
            return null;

        }

        int x = (left.x - left.y + right.x + right.y) / 2;
        int y = right.x + right.y - x;

        if (x >=n || y >= n || x < 0 || y < 0) {
            return null;
        }

        return new Position(x, y);
    }

    private static boolean isWhite(int i, int j) {
        return (i + j) % 2 == 0;
    }

    private static long rightDiagonalSum(int[][] arr, int n, int i, int j) {
        long sum = 0;

        while (i < n && j >= 0) {
            sum += arr[i][j];
            i++;
            j--;
        }

        return sum;
    }

    private static long leftDiagonalSum(int[][] arr, int n, int i, int j) {

        long sum = 0;

        while (i < n && j < n) {
            sum += arr[i][j];
            i++;
            j++;
        }

        return sum;
    }

}

class Position {
    public int x;
    public int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
