package random_solving;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by amayorov on 05.05.15.
 */
public class Codeforces339C {

	public static int filter(int notEqualsTo, int greaterThan) {
		for(int i = greaterThan; i < str.length(); ++i) {
			if ((str.charAt(i) == '1') && ((notEqualsTo == -1) || (i + 1 != notEqualsTo))) {
				return i + 1;
			}
		}

		return -1;
	}

	public static String str;

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		str = sc.next();
		int m = sc.nextInt();

		if (m == 1) {
			int b = str.lastIndexOf('1') + 1;
			if (b == 0) {
				System.out.println("NO");
				return;
			}
			System.out.println("YES");
			System.out.println(b);
			return;
		}


		List<Integer> result = new ArrayList<>();

		int a = 0;
		int b = 0;
		int last = 0;

		for(int i = 0; i < 10; ++i) {

			if (str.charAt(i) != '1') continue;

			for(int j = i + 1; j < 10; ++j) {
				if ((i == j) || (str.charAt(j) != '1')) continue;

				result.add(i + 1);
				result.add(j + 1);
				a = i + 1;
				b = j + 1;
				last = b;

				for(int k = 0; k < m - 2; ++k) {
					if (a <= b) {
						int n = filter(last, Math.abs(b - a));
						if (n == -1) {
							result.clear();
							break;
						}
						a += Math.signum(b - a) * n;
						result.add(n);
						last = n;
					} else {
						int n = filter(last, Math.abs(a - b));
						if (n == -1) {
							result.clear();
							break;
						}
						b += Math.signum(a-b) * n;
						result.add(n);
						last = n;
					}
				}

				if (!result.isEmpty()) {
					System.out.println("YES");
					for(Integer r: result) {
						System.out.print(r + " ");
					}
					return;
				}
			}


		}

		System.out.println("NO");

	}

}
