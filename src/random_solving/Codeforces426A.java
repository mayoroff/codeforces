package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 06.07.15.
 */
public class Codeforces426A {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int s = sc.nextInt();

		int[] a = new int[n];

		int sum = 0;

		for(int i = 0; i < n; ++i) {
			a[i] = sc.nextInt();
			sum += a[i];
		}

		for(int i = 0; i < n; ++i) {
			if (sum - a[i] <= s) {
				System.out.println("YES");
				return;
			}
		}
		System.out.println("NO");
	}

}
