package random_solving;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created by amayorov on 30.04.15.
 * Codeforces Round #197 (Div. 2) Problem A
 */
public class Codeforces339A {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.nextLine();

		ArrayList<Integer> arr = new ArrayList<>();

		for(int i = 0; i < s.length(); i += 2) {
			arr.add(Character.digit(s.charAt(i), 10));
		}

		arr.sort(Comparator.<Integer>naturalOrder());

		for(int i = 0; i < arr.size() - 1; ++i) {
			System.out.print(arr.get(i) + "+");
		}
		System.out.println(arr.get(arr.size() - 1));
	}

}
