package random_solving;

import java.util.Scanner;

/**
 * Created by Arseni on 13.07.2014.
 */
public class Codeforces447C {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        int[] arr = new int[n];

        for(int i = 0; i < n; ++i) {
            arr[i] = sc.nextInt();
        }

        //Fun begins
        int[] p = new int[n];
        int[] m = new int[n+1];

        int l = 0;

        for(int i = 0; i < n; ++i) {
            int lo = 1;
            int hi = l;

            while (lo <= hi) {
                int mid = (lo + hi)/2;
                if (arr[m[mid]] < arr[i]) {
                    lo = mid + 1;
                } else {
                    hi = mid - 1;
                }
            }

            int newL = lo;
            p[i] = m[newL - 1];

            if (newL > l) {
                m[newL] = i;
                l = newL;
            } else if(arr[i] < arr[m[newL]]) {
                m[newL] = i;
            }
        }

        System.out.println(l);

    }

}
