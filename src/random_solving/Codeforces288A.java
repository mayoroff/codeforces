package random_solving;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by amayorov on 18.05.15.
 */
public class Codeforces288A {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int k = sc.nextInt();

		List<Integer> points = new ArrayList<>();

		for (int i = 0; i <= 2*n; i++) {
			points.add(sc.nextInt());
		}

		for(int i = 1; i < points.size() && k > 0; i+=2) {
			int floor = Math.max(points.get(i-1), points.get(i+1));
			if (points.get(i) - 1 > floor) {
				points.set(i, (points.get(i) - 1));
				k--;
			}
		}

		points.stream().forEach(p -> System.out.print(p + " "));
	}

}
