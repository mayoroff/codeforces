package random_solving;

import java.util.Scanner;

/**
 * Created by Arseni on 11.07.2014.
 */
public class Codeforces266B {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int t = sc.nextInt();
        sc.nextLine();
        String str = sc.nextLine();
        for(int i = 0; i < t; ++i) {
            str = str.replace("BG", "GB");
        }

        System.out.println(str);
    }

}
