package random_solving;

import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by Arseni on 11.07.2014.
 */
public class Codeforces435A {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();

        LinkedList<Integer> groups = new LinkedList<>();

        for(int i = 0; i < n; ++i) {
            groups.add(sc.nextInt());
        }

        int k = 0;
        while (!groups.isEmpty()) {
            int c = m;
            k++;
            while (!groups.isEmpty() && (c >= groups.getFirst())) {
                c -= groups.removeFirst();
            }
        }
        System.out.println(k);
    }

}
