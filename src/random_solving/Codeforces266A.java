package random_solving;

import java.util.Scanner;

/**
 * Created by Arseny Mayorov on 05.10.2014.
 */
public class Codeforces266A {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        sc.nextLine();
        String str = sc.nextLine();

        int skip = 0;
        char c = str.charAt(0);
        for(int i = 1; i < n; ++i) {
            if (str.charAt(i) == c) {
                skip++;
            } else {
                c = str.charAt(i);
            }
        }

        System.out.println(skip);
    }

}
