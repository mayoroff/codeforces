package random_solving;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by amayorov on 05.09.2014.
 */
public class Codeforces372A {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();

		int[] arr = new int[n];

		for(int i = 0; i < n; ++i) {
			arr[i] = sc.nextInt();
		}

		Arrays.sort(arr);

		int i = 0;
		int k = arr.length / 2;
		int j = k;

		int result = n;

		while (i != k && j != arr.length) {
			if (arr[i] * 2 <= arr[j]) {
				result -= 1;
				i++;
			}
			j++;
		}

		System.out.println(result);
	}

}
