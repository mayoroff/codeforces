package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 08.09.2014.
 */
public class Codeforces465B {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();

		int last = 0;
		int actions = 0;
		for(int i = 0; i < n; ++i) {
			int k = sc.nextInt();
			if (k == 0 && last == 1) {
				actions++;
			}
			if (k == 1) {
				actions++;
			}
			last = k;
		}

		if (actions > 1 && last == 0) {
			actions--;
		}

		System.out.println(actions);
	}

}
