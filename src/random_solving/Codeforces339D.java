package random_solving;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import static java.lang.Math.abs;

/**
 * Created by amayorov on 06.05.15.
 */
public class Codeforces339D {

	public static void generate(int n, int m) {
		System.out.println(n + " " + m);
		Random rnd = new Random();
		int end = (int)Math.pow(2, n);
		for(int i = 0; i < end; ++i) {
			System.out.print(abs(rnd.nextInt()) + " ");
		}
		System.out.println("");
		for(int i = 0; i < m; ++i) {
			System.out.println(abs(rnd.nextInt(end)) + " " + abs(rnd.nextInt()));
		}
	}

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(System.in);

		if (args.length == 1 && args[0].equals("file")) {
			sc = new Scanner(new FileInputStream("test.txt"));
		}

		List<List<Integer>> treeLevels = new ArrayList<>();

		//long startTime = System.currentTimeMillis();
		//System.out.println("Start time: " + startTime);

		int n = sc.nextInt();
		int m = sc.nextInt();

		if (args.length == 1 && args[0].equals("generate")) {
			generate(n, m);
			return;
		}

		treeLevels.add(new ArrayList<>());
		int end = (int)Math.pow(2, n);
		for(int i = 0; i < end; ++i) {
			treeLevels.get(0).add(sc.nextInt());
		}

		//build tree
		for(int i = 1; i <= n; ++i) {
			treeLevels.add(new ArrayList<>());
			for(int k = 0; k < treeLevels.get(i-1).size(); k+=2) {
				if (i % 2 == 1) {
					treeLevels.get(i).add(treeLevels.get(i-1).get(k) | treeLevels.get(i-1).get(k + 1));
				} else {
					treeLevels.get(i).add(treeLevels.get(i-1).get(k) ^ treeLevels.get(i-1).get(k + 1));
				}
			}
		}

		//System.out.println(treeLevels.get(n).get(0));


		//serve requests

		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < m; ++i) {
			int p = sc.nextInt() - 1;
			int b = sc.nextInt();

			treeLevels.get(0).set(p, b);

			//update tree
			for(int l = 1; l <= n; ++l) {
				int newP = p >>> 1;

				int neighbourPos = p % 2 == 1 ? p - 1 : p + 1;

				int newResult;
				if (l % 2 == 1) {
					newResult = treeLevels.get(l-1).get(p) | treeLevels.get(l-1).get(neighbourPos);
				} else {
					newResult = treeLevels.get(l-1).get(p) ^ treeLevels.get(l-1).get(neighbourPos);
				}
				treeLevels.get(l).set(newP, newResult);
				p = newP;
			}

			//System.out.println(treeLevels.get(n).get(0));
			sb.append(treeLevels.get(n).get(0)).append('\n');
		}
		System.out.println(sb.toString());

		//long stopTime = System.currentTimeMillis();
		//System.out.println("Stop time: " + stopTime);
		//System.out.println("Run time: " + (stopTime - startTime));


	}

}

