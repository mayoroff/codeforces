package random_solving;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Arseni on 13.07.2014.
 */
public class Codeforces447CV2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        int[] arr = new int[n];

        for(int i = 0; i < n; ++i) {
            arr[i] = sc.nextInt();
        }

        if (n < 3) {
            System.out.println(n);
            return;
        }

        Integer lastElem = arr[0];
        Integer currL = 1;

        ArrayList<Integer> splits = new ArrayList<>();

        for(int i = 1; i < n; ++i) {

            try {
                if (arr[i - 2] + 1 >= arr[i]) {
                    //System.out.println("test");
                    splits.add(currL);
                    currL = 1;
                    continue;
                }
            } catch (Exception e) {
                //say nothing
            }

            if (arr[i] <= arr[i-1]) {
                splits.add(currL);

                /*if ((currL > 0) && (i > 1) && (arr[i] == lastElem + 1)) {
                    splits.add(currL);
                    System.out.println("test" + currL);
                }*/

                currL = 0;
            }
            currL = currL + 1;


        }
        splits.add(currL);

        System.out.println(Arrays.toString(splits.toArray()));

        if (splits.size() > 1) {
            int max = 0;
            for(int i = 0; i < splits.size() -1; ++i) {
                max = Math.max(max, splits.get(i) + splits.get(i + 1));
            }
            System.out.println(max);
        } else {
            System.out.println(splits.get(0));
        }

    }

}
