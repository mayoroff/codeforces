package random_solving;

import java.util.Scanner;

/**
 * Created by Arseni on 13.07.2014.
 */
public class Codeforces447A {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int p = sc.nextInt();
        int n = sc.nextInt();

        boolean[] arr = new boolean[p];

        for(int i = 0; i < n; ++i) {
            int c = sc.nextInt();
            if (arr[c % p]) {
                System.out.println(i + 1);
                return;
            }
            arr[c % p] = true;
        }

        System.out.println("-1");

    }

}
