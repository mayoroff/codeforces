package random_solving;

import java.util.Scanner;

/**
 * Created by Arseni on 19.07.2014.
 */
public class Codeforces448A {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int[] a = new int[n];

        for(int i = 0; i < n; ++i) {
            int k = sc.nextInt();
            a[i] = (k / m) + ((k % m == 0) ? 0 : 1);
        }

        int max = a[0];
        int maxPos = 1;
        for(int i = 1; i < n; ++i) {
            if (a[i] >= max) {
                max = a[i];
                maxPos = i + 1;
            }
        }
        System.out.println(maxPos);
    }

}
