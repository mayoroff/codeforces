package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 05.09.2014.
 */
public class Codeforces463A {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int s = sc.nextInt();

		int result = -1;

		for(int i = 0; i < n; ++i) {
			int d = sc.nextInt();
			int c = sc.nextInt();

			if ((d < s) || (d == s && c == 0)) {
				result = Math.max(result, 100 - c) == 100 ? Math.max(result, 0) : Math.max(result, 100 - c);
			}
		}

		System.out.println(result);
	}

}
