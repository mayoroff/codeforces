package random_solving;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by amayorov on 16.01.2015.
 */
public class Codeforces501B {

	static Map<String, String> storage = new HashMap<>();

	public static void processRequest(String old, String n) {

		if (old.equals(n)) {
			return;
		}

		String str;
		if ((str = storage.get(old)) != null) {
			storage.remove(old);
			storage.put(n, str);
		} else {
			storage.put(n, old);
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);


		int q = sc.nextInt();

		for (int i = 0; i < q; i++) {
			String str1 = sc.next();
			String str2 = sc.next();
			processRequest(str1, str2);
		}

		System.out.println(storage.size());

		for(Map.Entry<String, String> e: storage.entrySet()) {
			System.out.println(e.getValue() + " " + e.getKey());
		}


	}

}
