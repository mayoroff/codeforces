package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 16.01.2015.
 */
public class Codeforces501A {

	public static int calc(int p, int t) {
		int a = 3 * p / 10;
		int b = p - (p / 250 * t);
		return Math.max(a, b);
	}

	public static void main(String[] args) {
		int a, b, c, d;

		Scanner sc = new Scanner(System.in);

		a = sc.nextInt();
		b = sc.nextInt();
		c = sc.nextInt();
		d = sc.nextInt();

		int misha = calc(a, c);
		int vasya = calc(b, d);

		if (misha > vasya) {
			System.out.println("Misha");
		} else if (vasya > misha) {
			System.out.println("Vasya");
		} else {
			System.out.println("Tie");
		}

	}

}
