package random_solving;

import java.util.Scanner;

/**
 * Created by Arseny Mayorov on 13.02.2015.
 */
public class Codeforces1B {

    static String convertRXCYtoAA(String rxcy) {

        String strNumber = rxcy.substring(rxcy.indexOf('C') + 1);
        int number = Integer.parseInt(strNumber) - 1;

        StringBuilder sb = new StringBuilder();

        int rem = number % 26;
        number = number / 26 - 1;
        sb.append((char)((int)'A' + rem));

        while (number != -1) {
            rem = number % 26;
            number = number / 26 - 1;

            char symbol = (char)((int)'A' + rem);
            sb.append(symbol);
        }

        sb = sb.reverse();
        strNumber = rxcy.substring(1, rxcy.indexOf('C'));
        sb.append(strNumber);

        return sb.toString();
    }

    static String convertAAtoRXCY(String aa) {

        StringBuilder sb = new StringBuilder();
        sb.append("R");

        int i = 0;
        for(char c : aa.toCharArray()) {
            if (Character.isDigit(c)) {
                break;
            }
            i++;
        }

        sb.append(aa.substring(i));
        sb.append("C");

        String letters = aa.substring(0, i);
        int number = 0;
        int e = 1;
        for (int j = letters.length() - 1; j >= 0; --j) {
            number += e * (letters.charAt(j) - 'A' + 1);
            e *= 26;
        }

        sb.append(number);

        return sb.toString();
    }

    static boolean isAA(String str) {
        boolean shouldBeDigit = false;
        for(char c : str.toCharArray()) {

            if (!shouldBeDigit && Character.isDigit(c)) {
                shouldBeDigit = true;
                continue;
            }

            if (shouldBeDigit && Character.isLetter(c)) {
                return false;
            }
        }

        return true;
    }

    static String process(String str) {
        if (isAA(str)) {
            return convertAAtoRXCY(str);
        } else {
            return convertRXCYtoAA(str);
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        sc.nextLine();

        for(int i = 0; i < n; ++i) {
           String str = sc.nextLine();
            System.out.println(process(str));
        }
    }

}
