package random_solving;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created by amayorov on 20.08.2014.
 */
//It's nice to try Java 8 here

public class Codeforces158A {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int k = sc.nextInt();

		List<Integer> scores = new ArrayList<>(n);

		for(int i = 0; i < n; ++i) {
			scores.add(sc.nextInt());
		}

		int limit = scores.get(k - 1);

		System.out.println(scores.stream().filter(p -> p > 0 && p >= limit).count());
	}

}
