package random_solving;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by amayorov on 12.05.15.
 */
public class Codeforces82C {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int[] a = new int[n+1];
		for(int i = 1; i <= n; ++i) {
			a[i] = sc.nextInt();
		}

		ArrayList<ArrayList<Path>> g = new ArrayList<>();
		for(int i = 0; i <= n; ++i) {
			g.add(new ArrayList<>());
		}

		for(int i = 0; i < n - 1; ++i) {
			int start = sc.nextInt();
			int end = sc.nextInt();
			int c = sc.nextInt();
			g.get(start).add(new Path(end, c));
			g.get(end).add(new Path(start, c));
		}



	}

}

class Path {
	int destination;
	int capacity;

	public Path(int destination, int capacity) {
		this.destination = destination;
		this.capacity = capacity;
	}
}
