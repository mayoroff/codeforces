package random_solving;

import java.util.HashSet;
import java.util.Scanner;

public class Codeforces443A {
	public static void main(String[] args) {
	
		String line = new Scanner(System.in).nextLine();
		HashSet<Character> set = new HashSet<>();
	
	
		for(int i = 0; i < line.length(); ++i) {
			char a = line.charAt(i);
			switch(a) {
				case '{':
				case '}':
				case ' ':
				case ',':
					continue;
				default:
					set.add(a);
			}
		}
	
		System.out.println(set.size());
	
	}
}