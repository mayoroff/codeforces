package random_solving;

import java.util.Scanner;

/**
 * Created by Arseni on 19.07.2014.
 */
public class Codeforces448C {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int k = sc.nextInt();

        if (m > n) {
            int tmp = n;
            n = m;
            m = tmp;
        }

        if (n / k == m / k && (n / k) < 10) {
            int tmp = n;
            n = m;
            m = tmp;
        }

        if (n > k) {
            n = n / (k + 1);
            k = 0;
        } else {
            k = k - Math.min(n - 1, k);
            n = 1;
        }

        //System.out.println("n =" + n);
       //System.out.println("k =" + k);


        if (k != 0) {
            if (m > k) {
                m = m / (k + 1);
                k = 0;
            } else {
                k = k - Math.min(m - 1, k);
                m = 1;
            }
        }

        //System.out.println("m =" + m);
        //System.out.println("k =" + k);


        if (k != 0) {
            System.out.println(-1);
        } else {
            long c = (long) m * (long) n;
            System.out.println(c);
        }

    }

}
