package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 09.09.2014.
 */
public class Codeforces376A {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();

		int middle = str.indexOf("^");

		long leftF = 0;
		long rightF = 0;

		int s = middle;

		for(int i = 0; i < middle; ++i) {
			if (str.charAt(i) != '=') {
				leftF += Character.getNumericValue(str.charAt(i)) * s;
			}
			s--;
		}

		s = 1;
		for(int i = middle + 1; i < str.length(); ++i) {
			if (str.charAt(i) != '=') {
				rightF += Character.getNumericValue(str.charAt(i)) * s;
			}
			s++;
		}

		if (rightF == leftF) {
			System.out.println("balance");
		} else if (rightF > leftF) {
			System.out.println("right");
		} else {
			System.out.println("left");
		}
	}

}
