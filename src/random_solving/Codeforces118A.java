package random_solving;

import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by Arseny Mayorov on 05.10.2014.
 */
public class Codeforces118A {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String str = sc.nextLine();

        HashSet<Character> vowels = new HashSet<>();
        vowels.add('A');
        vowels.add('O');
        vowels.add('Y');
        vowels.add('E');
        vowels.add('U');
        vowels.add('I');

        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < str.length(); ++i) {
            char c = str.charAt(i);
            if (!vowels.contains(Character.toUpperCase(c))) {
                sb.append('.');
                sb.append(Character.toLowerCase(c));
            }
        }

        System.out.println(sb.toString());
    }

}
