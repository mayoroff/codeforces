package random_solving;

import java.util.Scanner;

/**
 * Created by Arseni on 19.07.2014.
 */
public class Codeforces448E {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        if (n < 4) {
            System.out.println(0);
            return;
        }

        StringBuilder sb = new StringBuilder();
        int k = 0;

        boolean[] numbers = new boolean[n+1];

        int last = -1;
        for(int i = 2; i <= n/2; ++i) {
            for(int j = 2; j <= n; ++j) {
                if (numbers[j]) {
                    continue;
                }
                if (j % i == 0) {
                    if (last == -1) {
                        last = j;
                    } else
                    {
                        sb.append(last).append(" ").append(j).append('\n');
                        k++;
                        numbers[last] = true;
                        numbers[j] = true;
                        last = -1;
                    }
                }
            }
            last = -1;
        }
        System.out.println(k);
        System.out.println(sb.toString());
    }

}
