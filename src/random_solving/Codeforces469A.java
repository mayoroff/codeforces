package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 26.09.2014.
 */
public class Codeforces469A {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();

		boolean[] arr = new boolean[n];

		int p = sc.nextInt();
		for(int i = 0; i < p; ++i) {
			int k = sc.nextInt();
			arr[k-1] = true;
		}

		p = sc.nextInt();
		for(int i = 0; i < p; ++i) {
			int k = sc.nextInt();
			arr[k-1] = true;
		}

		for(int i = 0; i < n; ++i) {
			if (arr[i] == false) {
				System.out.println("Oh, my keyboard!");
				return;
			}
		}
		System.out.println("I become the guy.");

	}

}
