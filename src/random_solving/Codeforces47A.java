package random_solving;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by amayorov on 20.08.2014.
 */
public class Codeforces47A {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();


		Set<Integer> triangleNumbers = calculateTriangleNumbersUntil(n);

		if (triangleNumbers.contains(n)) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}
	}

	public static Set<Integer> calculateTriangleNumbersUntil(int n) {

		Set<Integer> result = new HashSet<>(n / 4 + 1);

		int t = 1;
		int number = 1;
		result.add(number);

		while (number < n) {
			t++;
			number = t * (t+1) / 2;
			result.add(number);
		}

		return result;
	}
}
