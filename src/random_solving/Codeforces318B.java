package random_solving;

import java.util.Scanner;

/**
 * Created by amayorov on 10.09.2014.
 */
public class Codeforces318B {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();

		int st_index = str.indexOf("heavy", 0);
		int end_index = st_index;

		long points = 0;
		long sts = 1;

		int next_st = 0;

		while (true) {
			int next_end = str.indexOf("metal", end_index + 5);

			if (next_st != -1) {
				next_st = str.indexOf("heavy", st_index + 5);
			}

			if (next_end == -1) {
				break;
			}

			if (next_st == -1 || next_end < next_st) {
				points += sts;
				end_index = next_end;
			} else {
				st_index = next_st;
				sts++;
			}
		}

		System.out.println(points);
	}

}
