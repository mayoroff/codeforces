package codeforces260;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by amayorov on 08.08.2014.
 */
public class Codeforces456B {


	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		BigInteger n = new BigInteger(sc.nextLine());

		BigInteger a = new BigInteger("1");
		BigInteger b = new BigInteger("2").modPow(n, new BigInteger("5"));
		BigInteger c = new BigInteger("3").modPow(n, new BigInteger("5"));
		BigInteger d = new BigInteger("4").modPow(n, new BigInteger("5"));

		BigInteger result = a.add(b).add(c).add(d).mod(new BigInteger("5"));
		System.out.println(result);

	}

}
