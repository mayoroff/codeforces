package codeforces260;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * Created by amayorov on 08.08.2014.
 */
public class Codeforces456C {

	public static class MyComparator implements Comparator<CountNumberPair>
	{
		public int compare( CountNumberPair x, CountNumberPair y )
		{
			return new Long(y.score).compareTo(x.score);
		}
	}

	public static class CountNumberPair {
		public long score;
		public int number;

		public CountNumberPair(long score, int number) {
			this.score = score;
			this.number = number;
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();

		if (n == 1) {
			System.out.println(sc.nextInt());
			return;
		}

		int[] seq = new int[n + 10];

		int[] c = new int[10100];

		for(int i = 0; i < n; ++i) {
			int num = sc.nextInt();
			seq[i] = num;
			c[num] += 1;
		}

		PriorityQueue<CountNumberPair> heap = new PriorityQueue<CountNumberPair>(n + 100, new MyComparator());

		long[] score = new long[10100];
		for(int i = 1; i <= n; ++i) {
			//TODO we may have overflow here
			score[i] = (long)c[i] * i - (long)c[i - 1] * (i - 1) - (long)c[i + 1] * (i + 1);
		}


		for(int i = 0; i < n; ++i) {
			heap.add(new CountNumberPair(score[i + 1], i + 1));
		}

		long totalScore = 0;

		while (!heap.isEmpty()) {
			int number = heap.poll().number;
			long score1 = heap.poll().score;

			if (c[number] == 0) {
				continue;
			}

			totalScore += (long)c[number] * number;

			c[number - 1] = 0;
			c[number + 1] = 0;

			//recount score
			int nPrevPrev = number - 2;

			if ((nPrevPrev > 0) && (nPrevPrev < n)) {

				long scoreNPrevPrev = (long) c[nPrevPrev] * nPrevPrev - (long) c[nPrevPrev - 1] * (nPrevPrev - 1) - (long) c[nPrevPrev + 1] * (nPrevPrev + 1);
				if (scoreNPrevPrev > score[nPrevPrev]) {
					score[nPrevPrev] = scoreNPrevPrev;
					heap.add(new CountNumberPair(scoreNPrevPrev, nPrevPrev));
				}

			}

			int nNextNext = number + 2;

			if ((nNextNext > 0) && (nNextNext < n)) {

				long scoreNNextNext = (long) c[nNextNext] * nNextNext - (long) c[nNextNext - 1] * (nNextNext - 1) - (long) c[nNextNext + 1] * (nNextNext + 1);
				if (scoreNNextNext > score[nNextNext]) {
					score[nNextNext] = scoreNNextNext;
					heap.add(new CountNumberPair(scoreNNextNext, nNextNext));
				}
			}

		}

		System.out.println(totalScore);

	}

}
