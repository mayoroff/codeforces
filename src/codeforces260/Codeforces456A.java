package codeforces260;

import java.util.Scanner;

/**
 * Created by amayorov on 08.08.2014.
 */
public class Codeforces456A {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();

		int[] arr = new int[n];

		for(int i = 0; i < n; ++i) {
			int a = sc.nextInt();
			int b = sc.nextInt();
			arr[a-1] = b;
 		}

		int k = arr[0];
		for(int i = 1; i < n; ++i) {
			if (k > arr[i]) {
				System.out.println("Happy Alex");
				return;
			}
			k = arr[i];
		}

		System.out.println("Poor Alex");
	}

}
