package codeforces262;

import java.util.Scanner;

/**
 * Created by amayorov on 20.08.2014.
 */
public class Codeforces460A {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int m = sc.nextInt();

		int day = 1;
		while (n > 0) {
			n--;
			if (day % m == 0) {
				n++;
			}
			day++;
		}

		System.out.println(day - 1);
	}

}
