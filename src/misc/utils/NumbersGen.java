package misc.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by amayorov on 09.09.2014.
 */
public class NumbersGen {

	public static long LOWEST = 0;
	public static long HIGHEST = (long) Math.pow(10, 9);

	public static int N = 100000;

	static List<Long> randomNumbers() {
		Random random = new Random();
		List<Long> result = new ArrayList<>(N);

		for(int i = 0; i < N; ++i) {
			long rnd = -1;
			while (rnd < 0) {
				rnd = random.nextLong();
			}
			result.add(LOWEST + (rnd % (HIGHEST - LOWEST + 1)));
		}

		return result;
	}

	static List<Long> constantNumbers(long k) {
		Random random = new Random();
		List<Long> result = new ArrayList<>(N);

		for(int i = 0; i < N; ++i) {
			result.add(k);
		}

		return result;
	}

	public static void main(String[] args) {
		System.out.println(N);
		for(Long l: randomNumbers()) {
			System.out.print(l + " ");
		}
	}

}
