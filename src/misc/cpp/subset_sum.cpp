#include <iostream>
#include <vector>

using namespace std;

vector<vector<bool>> q;

bool qfunc(int i, int s, vector<int>& x) {

	if (i >= q.size() || (s < 0)) {
		return false;
	}

	if (x[i] == s) {
		cout << x[i] << " ";
		return true;
	} 

	if (qfunc(i + 1, s, x)) {
		return true;
	}

	if (qfunc(i + 1, s - x[i], x)) {
		cout << x[i] << " ";
		return true;
	}

	return false;
}

int main(void) {

	int nums[12] = {25, 23, 21, 19, 3, 5, 7, 9, 11, 13, 15, 17};
	vector<int> vNums(nums, nums + 12);

	int target = 83;

	const int MAX_NUM = 200;

	for(size_t i = 0; i < vNums.size(); ++i) {
		q.push_back(*(new vector<bool>(MAX_NUM)));
	}

	qfunc(0, target, vNums);

	return 0;
}