#include <iostream>

int main(void) {
	int n = 0;
	std::cin >> n;


	for(int i=2; i < 100; i=i+2) {
		for(int j=2; j < 100; j=j+2) {
			if ((i + j) == n) {
				std::cout << "YES" << std::endl;
				return 0;
			}
		}
	}

	std::cout << "NO" << std::endl;

	return 0;
}