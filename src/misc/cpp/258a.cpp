#include <iostream>
#include <string>

int main(void) {

	std::string input;
	std::cin >> input;

	size_t pos = input.find_first_of('0');

	if (pos == std::string::npos) {
		std::cout << input.substr(1) << std::endl;
	} else {
		std::cout << input.substr(0, pos) + input.substr(pos + 1) << std::endl;
	}

	return 0;
}